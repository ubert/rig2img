pub fn min_max(samples: &Vec<i16>) -> (i16, i16) {
    let min = *samples.iter().min().unwrap_or(&0);
    let max = *samples.iter().max().unwrap_or(&0);
    (min, max)
}

pub fn to_pixel(sample: i16, max: i16) -> u8 {
    let float: f32 = (sample as f32 / max as f32).abs();
    (float * 255.) as u8
}
