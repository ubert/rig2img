extern crate argparse;
extern crate image;
extern crate hound;

mod lib;

use std::{process};
use lib::{min_max, to_pixel};
use argparse::{ArgumentParser, StoreTrue, Store};

fn main() {

    let mut verbose = false;
    let mut soundfile = String::new();
    let mut outdir = String::from(".");
    let mut filename = String::from("image.png");
    let mut width: u32 = 1000;
    let mut height: u32 = 1000;
    let mut fps: u16 = 25;

    {  // this block limits scope of borrows by ap.refer() method
        let mut ap = ArgumentParser::new();
        ap.set_description("Turn audio data into images");
        ap.refer(&mut width).add_option(
            &["-w", "--width"],
            Store,
            "image width"
        );
        ap.refer(&mut height).add_option(
            &["-h", "--height"],
            Store,
            "image height"
        );
        ap.refer(&mut fps).add_option(
            &["-f", "--fps"],
            Store,
            "frames per second"
        );
        ap.refer(&mut verbose).add_option(
            &["-v", "--verbose"],
            StoreTrue,
            "Be verbose"
        );
        ap.refer(&mut outdir).add_option(
            &["-o", "--outdir"],
            Store,
            "Where to write ouput to"
        );
        ap.refer(&mut soundfile).add_argument(
            "Soundfile",
            Store,
            "Soundfile to parse"
        ).required();
        ap.parse_args_or_exit();
    }

    let imagefile = [outdir, filename].join("/");
    if verbose {
        print_config(&soundfile, &width, &height, &fps, &imagefile);
    }

    let mut reader = hound::WavReader::open(soundfile).unwrap();
    let mut buffer = image::GrayImage::new(width, height);

    let spec:hound::WavSpec = reader.spec();
    if verbose {
        print_spec(spec, reader.duration());
    }

    let samples: Vec<i16> = reader.samples().map(|s| s.unwrap()).collect();
    if samples.len() < (width * height) as usize {
        eprintln!("Not enough samples to fill the pixelspace!");
        process::exit(1);
    }

    let (_min, max) = min_max(&samples);

    for (x, y, pixel) in buffer.enumerate_pixels_mut() {
        let pos = y * width + x;
        let val = samples[pos as usize] as i16;
        let pix = to_pixel(val, max);
        *pixel = image::Luma([pix as u8]);
    }
    buffer.save(imagefile).unwrap()
}

fn fmt_sample_format(sample_format:hound::SampleFormat) -> String {
    match sample_format {
        hound::SampleFormat::Float => String::from("Float"),
        hound::SampleFormat::Int => String::from("Integer"),
    }
}

fn print_config(soundfile: &String, width: &u32, height: &u32, fps: &u16, imagefile: &String) {
    println!("soundfile {}", soundfile);
    println!("format    {}x{}", width, height);
    println!("fps       {}", fps);
    println!("save to   {}", imagefile);
}

fn print_spec(spec: hound::WavSpec, duration: u32) {
    println!("-----------------------------------------------------");
    println!("duration    {}", duration);
    println!("channels    {}", spec.channels);
    println!("samplerate  {}", spec.sample_rate);
    println!("bits/sample {}", spec.bits_per_sample);
    println!("format      {}", fmt_sample_format(spec.sample_format));
}
